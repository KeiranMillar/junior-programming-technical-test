﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public GameObject snake;
    public GameObject bodyPrefab;
    public GameObject gameOverScreen;
    public GameObject scoreText;
    public GameObject HiScoreText;

    //Different movement directions the player can go
    enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    private Vector2Int position;
    public float moveTimerMax;
    private float moveTimer;
    public float moveTimerMultiplier;
    private Direction moveDirection;
    private Direction lastMoveDirection;
    public int bodySegments;
    public int HiScore;

    public List<Vector3> bodyPositions;
    private List<GameObject> bodyObjects;

    // Use this for initialization
    void Start ()
    {
        position = new Vector2Int(0,0);
        moveTimerMax = 1.0f;
        moveTimer = moveTimerMax;
        moveDirection = Direction.Up;
        lastMoveDirection = moveDirection;
        bodySegments = 0;
        HiScore = PlayerPrefs.GetInt("highscore");

        Time.timeScale = 1;
        
        scoreText.SetActive(true);
        HiScoreText.SetActive(true);
        gameOverScreen.SetActive(false);

        bodyPositions = new List<Vector3>();
        bodyObjects = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //If the player goes off the screen
        if (position.x >= 10 || position.x <= -10 || position.y >= 10 || position.y <= -10)
        {
            Die();
        }

        HandleInput();

        //Add delta time to counter
        moveTimer += Time.deltaTime;
        //If counter is higher than the move timer
        if(moveTimer >= moveTimerMax)
        {
            //Move the snake and do collision detection
            UpdateBodyPositions(this.transform.position);
            MoveSnake();
            BodyCollisionCheck();
            //Reset move timer
            moveTimer = 0;
        }
    }

    void HandleInput()
    {
        //Change direction the player will move based on input and the direction the snake last moved
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (lastMoveDirection != Direction.Down)
            {
                moveDirection = Direction.Up;
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (lastMoveDirection != Direction.Up)
            {
                moveDirection = Direction.Down;
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (lastMoveDirection != Direction.Left)
            {
                moveDirection = Direction.Right;
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (lastMoveDirection != Direction.Right)
            {
                moveDirection = Direction.Left;
            }
        }
    }

    void MoveSnake()
    {
        //Move character based on the direction the user is going
        switch (moveDirection)
        {
            case Direction.Up:
                position.y += 1;
                break;
            case Direction.Down:
                position.y -= 1;
                break;
            case Direction.Left:
                position.x -= 1;
                break;
            case Direction.Right:
                position.x += 1;
                break;
        }

        //Update position and save the direction you just moved for the next movement
        snake.transform.position = new Vector3(position.x, position.y, 0);
        lastMoveDirection = moveDirection;

        //Update the body parts of the snake
        UpdateBody();
    }

    void UpdateBodyPositions(Vector3 newPos)
    {
        //Add this new position to a list of old positions
        bodyPositions.Add(newPos);

        //Remove useless old positions that arent required
        while (bodyPositions.Count > bodySegments)
        {
            bodyPositions.RemoveAt(0);
        }
    }

    void UpdateBody()
    {
        for (int i = 0; i < bodySegments; i++)
        {
            //Move the body parts to their relevant locations
            bodyObjects[i].transform.position = bodyPositions[i];
        }
    }

    void AddBody()
    {
        //Instantiate new body segment and add it to list of segments 
        bodySegments++;
        GameObject newBody = Instantiate(bodyPrefab);
        bodyObjects.Add(newBody);
    }

    void BodyCollisionCheck()
    {
        //For each body segment
        for (int i = 0; i < bodySegments; i++)
        {
            //Check if the snake head has collided with it
            if(snake.transform.position == bodyPositions[i])
            {
                Die();
            }
        }
    }

    public void FoodEaten()
    {
        //When food is eaten add a new body segment
        AddBody();
        //Decrease time between steps
        moveTimerMax *= moveTimerMultiplier;
        //Check for new highscore
        if(bodySegments > HiScore)
        {
            HiScore = bodySegments;
        }
    }

    public void Restart()
    {
        //Reset all necessary variables and time scale
        position = new Vector2Int(0, 0);
        moveTimerMax = 1.0f;
        moveTimer = moveTimerMax;
        moveDirection = Direction.Up;
        lastMoveDirection = moveDirection;
        bodySegments = 0;

        Time.timeScale = 1;

        //Activate relevant UI elements and remove game over screen
        scoreText.SetActive(true);
        HiScoreText.SetActive(true);
        gameOverScreen.SetActive(false);

        //Destroy all body segments and reset lists for their information
        for (int i = 0; i < bodyObjects.Count; i++)
        {
            Destroy(bodyObjects[i]);
        }
        bodyObjects.Clear();

        bodyPositions = new List<Vector3>();
        bodyObjects = new List<GameObject>();
    }

    public void Die()
    {
        //Pause the game and replace game UI with game over screen
        Time.timeScale = 0;
        scoreText.SetActive(false);
        HiScoreText.SetActive(false);
        gameOverScreen.SetActive(true);
    }
}