﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour {

    public Movement score;
    public TMPro.TextMeshProUGUI scoreText;
    public TMPro.TextMeshProUGUI HiScoreText;

    public TMPro.TextMeshProUGUI HiScoreDeathText;

    // Update is called once per frame
    void Update ()
    {
        //These are used to display the score and high score on UI elements during gameplay
        scoreText.text = "Score:" + score.bodySegments;
        HiScoreText.text = "High Score:" + score.HiScore;

        //This displays the highest score on the death screen
        if(HiScoreDeathText.IsActive() == true)
        {
            HiScoreDeathText.text = score.HiScore.ToString();
        }
    }
}