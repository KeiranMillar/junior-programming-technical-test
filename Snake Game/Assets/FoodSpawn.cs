﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawn : MonoBehaviour {

    public GameObject food;
    public GameObject snakeHead;
    private Vector2Int pos;

    // Use this for initialization
    void Start ()
    {
        //Spawns food on game start
        SpawnFood();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //If the food has been eaten then spawn another
		if(!food.activeSelf)
        {
            SpawnFood();
        }
	}

    void SpawnFood()
    {
        Vector3Int randomPos = new Vector3Int(Random.Range(-9, 9), Random.Range(-9, 9), 0);
        Vector3 snakeHeadPos = new Vector3();
        List<Vector3> bodyPos = new List<Vector3>();

        //Get the positions of the body segments and the snake head
        bodyPos = snakeHead.GetComponent<Movement>().bodyPositions;
        snakeHeadPos = snakeHead.transform.position;

        //While the random position is the same as a snake body part
        while (!ValidPlacement(randomPos, snakeHeadPos, bodyPos))
        {
            //Generate another random number and check if its valid
            randomPos = new Vector3Int(Random.Range(-9, 9), Random.Range(-9, 9), 0);
        }

        //When a valid position is calculated move the food to that place and enable it
        food.transform.position = randomPos;
        food.SetActive(true);
    }

    bool ValidPlacement(Vector3Int randPos, Vector3 currentPos, List<Vector3> bodyPos)
    {
        //If the random position is the same as the snake head
        if (randPos.x == currentPos.x && randPos.y == currentPos.y)
        {
            //Not valid
            return false;
        }

        //For every body segment
        for (int i = 0; i < bodyPos.Count; i++)
        {
            //Check if the random position is the same as the body segment
            if (randPos.x == bodyPos[i].x && randPos.y == bodyPos[i].y)
            {
                //Not valid
                return false;
            }
        }

        //Is Valid
        return true;
    }
}
