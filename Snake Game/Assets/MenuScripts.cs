﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScripts : MonoBehaviour
{
    public GameObject snakeHead;

    public void BackToMenu()
    {
        PlayerPrefs.SetInt("highscore", snakeHead.GetComponent<Movement>().HiScore);
        SceneManager.LoadScene("Menu");
    }

    public void Replay()
    {
        snakeHead.GetComponent<Movement>().Restart();
    }
}