﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodPickUp : MonoBehaviour {

    public GameObject snakeHead;
	

	// Update is called once per frame
	void Update ()
    {
        //This checks to see if the snake eats the food
        if (snakeHead.transform.position == this.transform.position)
        {
            snakeHead.GetComponent<Movement>().FoodEaten();
            this.gameObject.SetActive(false);
        }
    }
}
